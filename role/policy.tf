###############################################################################
# Optional Variables
###############################################################################

variable "policy_map" {
  default     = {}
  description = "A map of policies to attach to this IAM role."

  type = map(object({
    arn        = optional(string)
    path       = optional(string, "/")
    account_id = optional(string)
  }))
}

################################################################################
# Locals
################################################################################

locals {
  policy_map = {
    for key, value in var.policy_map : key => {
      "arn"        = coalesce(value.arn, format("arn:aws:iam::%v:policy%s%s", value.account_id, value.path, key))
      "path"       = value.arn == null ? value.path : null
      "account_id" = value.arn == null ? coalesce(value.account_id, data.aws_caller_identity.scope.account_id) : null
    }
  }
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_role_policy_attachment" "scope" {
  for_each   = local.policy_map
  role       = aws_iam_role.scope.name
  policy_arn = each.value.arn
}

###############################################################################
# Outputs
###############################################################################

output "policy_map" {
  value = local.policy_map
}

###############################################################################

output "policy_ids" {
  value = {
    for key, value in aws_iam_role_policy_attachment.scope : key => value.id
  }
}

output "policy_arns" {
  value = {
    for key, value in aws_iam_role_policy_attachment.scope : key => value.policy_arn
  }
}

output "policy_roles" {
  value = {
    for key, value in aws_iam_role_policy_attachment.scope : key => value.role
  }
}

###############################################################################
