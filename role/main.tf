###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the IAM role to create."
}

variable "assume_policy" {
  type        = string
  description = "The policy document that grants an entity permission to assume the role in JSON format."
}

###############################################################################
# Optional Variables
###############################################################################

variable "path" {
  type        = string
  default     = "/"
  description = "The IAM path to the role."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the role."
}

variable "force_detach" {
  type        = bool
  default     = false
  description = "Specifies to force detaching any policies the role has before destroying it."
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_role" "scope" {
  name                  = var.name
  path                  = var.path
  description           = var.description
  assume_role_policy    = var.assume_policy
  force_detach_policies = var.force_detach
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = aws_iam_role.scope.unique_id
}

output "arn" {
  value = aws_iam_role.scope.arn
}

output "name" {
  value = aws_iam_role.scope.name
}

output "path" {
  value = aws_iam_role.scope.path
}

output "created" {
  value = aws_iam_role.scope.create_date
}

output "assume_role" {
  value = aws_iam_role.scope.assume_role_policy
}

output "description" {
  value = aws_iam_role.scope.description
}

output "force_detach" {
  value = aws_iam_role.scope.force_detach_policies
}

###############################################################################
