###############################################################################
# Data Sources
###############################################################################

data "aws_caller_identity" "scope" {}

###############################################################################
# Outputs
###############################################################################

output "caller_identity_id" {
  value = data.aws_caller_identity.scope.user_id
}

output "caller_identity_arn" {
  value = data.aws_caller_identity.scope.arn
}

output "caller_identity_account" {
  value = data.aws_caller_identity.scope.account_id
}

###############################################################################
