<!---------------------------------------------------------------------------->

# iam/role

#### Manage [IAM] roles with [IAM] policies optionally attached

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/aws//role`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_iam_role" {
  source        = "gitlab.com/bitservices/identity/aws//role"
  name          = "MyRole"
  assume_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::012345678901:user/username"
      },
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
}
POLICY
}
```

<!---------------------------------------------------------------------------->

[IAM]: https://aws.amazon.com/iam

<!---------------------------------------------------------------------------->
