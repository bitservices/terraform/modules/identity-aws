<!---------------------------------------------------------------------------->

# iam/profile

#### Manage [IAM] instance profiles

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/aws//profile`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_iam_role" {
  source = "gitlab.com/bitservices/identity/aws//role/detached"
  name   = "MyRole"
  policy = <<POLICY
{
  "Version"  : "2012-10-17",
  "Statement": [
    {
      "Sid"      : "1",
      "Effect"   : "Allow",
      "Action"   : "sts:AssumeRole",
      "Principal": { "Service": "ec2.amazonaws.com" }
    }
  ]
}
POLICY
}

module "my_iam_profile" {
  source = "gitlab.com/bitservices/identity/aws//profile"
  role   = module.my_iam_role.name
}
```

<!---------------------------------------------------------------------------->

[IAM]: https://aws.amazon.com/iam

<!---------------------------------------------------------------------------->
