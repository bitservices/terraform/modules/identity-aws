###############################################################################
# Required Variables
###############################################################################

variable "role" {
  type        = string
  description = "The name of the IAM role to attach to this IAM instance profile."
}

###############################################################################
# Optional Variables
###############################################################################

variable "name" {
  type        = string
  default     = null
  description = "The full name of the IAM instance profile to create. Default is the same value as 'role'."
}

variable "path" {
  type        = string
  default     = "/"
  description = "The IAM path to the instance profile."
}

###############################################################################
# Locals
###############################################################################

locals {
  name = coalesce(var.name, var.role)
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_instance_profile" "scope" {
  name = local.name
  path = var.path
  role = var.role
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = aws_iam_instance_profile.scope.unique_id
}

output "arn" {
  value = aws_iam_instance_profile.scope.arn
}

output "name" {
  value = aws_iam_instance_profile.scope.name
}

output "path" {
  value = aws_iam_instance_profile.scope.path
}

output "role" {
  value = aws_iam_instance_profile.scope.role
}

###############################################################################
