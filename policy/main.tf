###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the IAM policy to create."
}

variable "policy" {
  type        = string
  description = "The policy document in JSON format."
}

###############################################################################
# Optional Variables
###############################################################################

variable "path" {
  type        = string
  default     = "/"
  description = "The IAM path to the policy."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the policy."
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_policy" "scope" {
  name        = var.name
  path        = var.path
  policy      = var.policy
  description = var.description
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = aws_iam_policy.scope.id
}

output "arn" {
  value = aws_iam_policy.scope.arn
}

output "name" {
  value = aws_iam_policy.scope.name
}

output "path" {
  value = aws_iam_policy.scope.path
}

output "policy" {
  value = aws_iam_policy.scope.policy
}

output "description" {
  value = aws_iam_policy.scope.description
}

###############################################################################
