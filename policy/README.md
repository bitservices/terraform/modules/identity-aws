<!---------------------------------------------------------------------------->

# iam/policy

#### Manage [IAM] policies

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/aws//policy`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_s3_bucket" {
  source  = "gitlab.com/bitservices/storage/aws//s3/bucket"
  owner   = "terraform@bitservices.io"
  prefix  = "foobar"
  company = "BITServices Ltd"
}

module "my_iam_policy" {
  source = "gitlab.com/bitservices/identity/aws//policy"
  name   = "MyPolicy"
  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement": [
    {
      "Sid":"MyReadOnlyBucketPermission",
      "Effect":"Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "${module.my_s3_bucket.arn}"
      ]
    },
    {
      "Sid":"MyReadOnlyBucketObjectsPermission",
      "Effect":"Allow",
      "Action": [
        "s3:GetObject",
        "s3:GetObjectVersion"
      ],
      "Resource": [
        "${module.my_s3_bucket.arn}/*"
      ]
    }
  ]
}
POLICY
}
```

<!---------------------------------------------------------------------------->

[IAM]: https://aws.amazon.com/iam

<!---------------------------------------------------------------------------->
