<!---------------------------------------------------------------------------->

# identity (aws)

<!---------------------------------------------------------------------------->

## Description

Manages [AWS] Identity and Access Management ([IAM]).

<!---------------------------------------------------------------------------->

## Modules

* [group](group/README.md) - Manage [IAM] groups.
* [policy](policy/README.md) - Manage [IAM] policies.
* [profile](profile/README.md) - Manage [IAM] instance profiles.
* [role](role/README.md) - Manage [IAM] roles with [IAM] policies optionally attached.
* [role-policy](role-policy/README.md) - Manage [IAM] role in-line policies.
* [user](user/README.md) - Manage [IAM] users.
* [user-policy](user-policy/README.md) - Manage [IAM] user in-line policies.

<!---------------------------------------------------------------------------->

[AWS]: https://aws.amazon.com/
[IAM]: https://aws.amazon.com/iam/

<!---------------------------------------------------------------------------->
