###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The name of the IAM role in-line policy to create."
}

variable "role" {
  type        = string
  description = "The IAM role to attach to the policy."
}

variable "policy" {
  type        = string
  description = "The policy document in JSON format."
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_role_policy" "scope" {
  name   = var.name
  role   = var.role
  policy = var.policy
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = aws_iam_role_policy.scope.id
}

output "name" {
  value = aws_iam_role_policy.scope.name
}

output "role" {
  value = aws_iam_role_policy.scope.role
}

output "policy" {
  value = aws_iam_role_policy.scope.policy
}

###############################################################################
