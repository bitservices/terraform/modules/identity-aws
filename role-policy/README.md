<!---------------------------------------------------------------------------->

# iam/role-policy

#### Manage [IAM] role in-line policies

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/aws//role-policy`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_iam_role" {
  source = "gitlab.com/bitservices/identity/aws//role/detached"
  name   = "MyRole"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::012345678901:user/username"
      },
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
}
POLICY
}

module "my_iam_role_policy" {
  source = "gitlab.com/bitservices/identity/aws//role-policy"
  name   = "MyRole-EC2"
  role   = module.my_iam_role.name
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "ec2:*",
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "elasticloadbalancing:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudwatch:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "autoscaling:*",
            "Resource": "*"
        }
    ]
}
POLICY
}
```

<!---------------------------------------------------------------------------->

[IAM]: https://aws.amazon.com/iam

<!---------------------------------------------------------------------------->
