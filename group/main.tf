###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the IAM group to create."
}

###############################################################################
# Optional Variables
###############################################################################

variable "path" {
  type        = string
  default     = "/"
  description = "The IAM path to the group."
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_group" "scope" {
  name = var.name
  path = var.path
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = aws_iam_group.scope.id
}

output "arn" {
  value = aws_iam_group.scope.arn
}

output "name" {
  value = aws_iam_group.scope.name
}

output "path" {
  value = aws_iam_group.scope.path
}

output "unique_id" {
  value = aws_iam_group.scope.unique_id
}

###############################################################################
