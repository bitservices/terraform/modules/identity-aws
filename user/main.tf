###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the IAM user to create."
}

###############################################################################
# Optional Variables
###############################################################################

variable "path" {
  type        = string
  default     = "/"
  description = "The IAM path to the user."
}

variable "force_destroy" {
  type        = bool
  default     = false
  description = "When true, destroy even if the user has non-Terraform-managed IAM access keys, login profile or MFA devices."
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_user" "scope" {
  name          = var.name
  path          = var.path
  force_destroy = var.force_destroy
}

###############################################################################
# Outputs
###############################################################################

output "path" {
  value = var.path
}

output "force_destroy" {
  value = var.force_destroy
}

###############################################################################

output "id" {
  value = aws_iam_user.scope.unique_id
}

output "arn" {
  value = aws_iam_user.scope.arn
}

output "name" {
  value = aws_iam_user.scope.name
}

###############################################################################
