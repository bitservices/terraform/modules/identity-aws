###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the IAM user in-line policy to create."
}

variable "user" {
  type        = string
  description = "The IAM user to attach to the policy."
}

variable "policy" {
  type        = string
  description = "The policy document in JSON format."
}

###############################################################################
# Resources
###############################################################################

resource "aws_iam_user_policy" "scope" {
  name   = var.name
  user   = var.user
  policy = var.policy
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = aws_iam_user_policy.scope.id
}

output "name" {
  value = aws_iam_user_policy.scope.name
}

output "user" {
  value = aws_iam_user_policy.scope.user
}

output "policy" {
  value = aws_iam_user_policy.scope.policy
}

###############################################################################
