<!---------------------------------------------------------------------------->

# iam/user-policy

#### Manage [IAM] user in-line policies

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/identity/aws//user-policy`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_iam_user" {
  source = "gitlab.com/bitservices/identity/aws//user"
  name   = "MyUser"
}

module "my_iam_user_policy" {
  source = "gitlab.com/bitservices/identity/aws//user-policy"
  name   = "MyUser-EC2"
  user   = module.my_iam_user.name
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeInstances",
        "ec2:DescribeNetworkAcls",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSubnets",
        "ec2:DescribeVolumes",
        "ec2:DescribeVpcs"
      ],
      "Resource": ["*"]
    }
  ]
}
POLICY
}
```

<!---------------------------------------------------------------------------->

[IAM]: https://aws.amazon.com/iam

<!---------------------------------------------------------------------------->
