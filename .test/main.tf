###############################################################################
# Locals
###############################################################################

locals {
  name = "foobar"

  assume_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::012345678901:user/username"
      },
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
}
POLICY

  principal_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Example permissions",
            "Effect": "Allow",
            "Action": "s3:*"
        }
    ]
}
POLICY
}

###############################################################################
